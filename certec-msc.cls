% certec-msc.cls
% Copyright 2013 Flavius Gruian <flavius.gruian@cs.lth.se>
% Modified 2016 by Hector Caltenco <hector.caltenco@certec.lth.se>
%
% Licensed under the GNU GPL version 3.0 or later.
% See http://www.gnu.org/licenses/gpl.html
%
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{certec-msc}
		[2015/01/16 v0.5 
		Masters Thesis Class, Certec, Dept. of Design Sciences, Lund University]

%\ExecuteOptions{12pt,a4paper,onecolumn, twoside, openright}
%\ProcessOptions\relax
%\LoadClass{report}
\LoadClass[12pt,a4paper,onecolumn,twoside,openright]{report}

% margins
% use showframe in parameters if you want to check the margins
\RequirePackage[left=2.5cm,right=2.5cm,top=3cm,bottom=3cm, bindingoffset=1cm]{geometry}

% Fonts! needs XeTeX
\RequirePackage{ifxetex}
\makeatletter
% for textbullets
\usepackage{textcomp}

   \ifxetex
		\usepackage[no-math]{fontspec}
	 	\setmainfont{TeX Gyre Termes}
	   	\setsansfont{TeX Gyre Heros}
    	\setmonofont{TeX Gyre Cursor}
	\else
		\ClassWarning{certec-msc}{Use XeTeX for better fonts!}		
		\usepackage[T1]{fontenc}
		\usepackage{qtxmath}	
		\usepackage{tgtermes}
		\usepackage{tgheros}
		\usepackage{tgcursor}

%		\InputIfFileExists{garamond.sty}{\AtBeginDocument{\garamond}}{%
%			\ClassError{certec-msc}{Missing garamond.sty. Unpack the zip from http://gael-varoquaux.info/computers/garamond/index.html in your source directory.}{}}
	
			
	\fi
\makeatother

% fancy headers, footers
\RequirePackage{fancyhdr}
\makeatletter
\pagestyle{fancy}
\renewcommand{\chaptermark}[1]{\markboth{\thechapter.\ #1}{}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}{}}
\fancyhf{}
\fancyhead[LE]{\footnotesize{\textsc{\leftmark}}}
\fancyhead[RO]{\footnotesize{\textsc{\rightmark}}}
\fancyfoot[LE,RO]{\thepage}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\fancypagestyle{plain}{
  \fancyhead{}
  \renewcommand{\headrulewidth}{0pt}
%  \renewcommand{\footrulewidth}{0pt}
}
\makeatother

% Title page
\RequirePackage{titling}

\makeatletter
    % custom commands
	\newcommand{\department}[1]{\def \@department {#1}}
	\newcommand{\supervisor}[1]{\def \@supervisor {#1}}
	\newcommand{\supervisors}[2]{\def \@firstsupervisor {#1}  \def \@secondsupervisor {#2}}
	\newcommand{\examiner}[1]{\def \@examiner {#1}}
	\newcommand{\subtitle}[1]{\def \@subtitle {#1}}
	\newcommand{\subject}[1]{\def \@subject {#1}}
	\department{}
	\supervisor{}
	\supervisors{}{}
	\examiner{}
	\subtitle{}
	\subject{}
	
	% for the title page
	\pretitle{\noindent\rule{\linewidth}{1pt}\begin{center}\Huge}
	\posttitle{\par \vskip 0.5em \ifx \@subtitle \empty \vskip 1em \else {\Large \@subtitle} \fi \end{center}\noindent\rule{\linewidth}{1pt}\vskip 0.5em}
	\predate{\vskip 3em \begin{center}\Large}
	\postdate{\par \vfill {
		\begin{figure}[!h] \centering \includegraphics[width=0.3\textwidth]{LundUniversity_C2line_BLACK.png} \end{figure}
		\large Master's thesis work carried out at 
		\ifx\@department\empty
		 	\ClassWarning{certec-msc}{Host department missing. Use \protect\department{name}. Defaulting to Certec.}
		 	\\ Certec, Dept. of Design Sciences, Lund University.
		\else \@department. \fi }
		  
			\vskip 1em 
			{\normalsize
			\ifx\@supervisor\empty 
			   %\ClassWarning{certec-msc}{Empty supervisor.}
			   \ifx\@firstsupervisor\empty
				  \ClassError{certec-msc}{Missing supervisor. Use either \protect\supervisor{name,email} or \protect\supervisors{name1,email1}{name2,email2}}{You must specify your thesis supervisor(s).}
				\else Supervisors: \@firstsupervisor ~\\ \@secondsupervisor \fi
			\else Supervisor: \@supervisor \fi
			
			Examiner: 
			\ifx\@examiner \empty
				\ClassError{certec-msc}{Missing examiner. Use \protect\examiner{name,email}}{You must specify your thesis examiner.}
			\else	 \@examiner \fi } \end{center}}

\makeatother


% Sections and captions
\RequirePackage{titlesec}
\makeatletter
	\titleformat{\chapter}[display]{\normalfont\huge\bfseries}
	{\chaptertitlename\ \thechapter}{10pt}{\Huge}[\vspace{2cm}\hrulefill]
	%\titleformat{\section}[hang]{\LARGE\bfseries\sffamily}{\thesection}{}
	\titleformat*{\section}{\LARGE\bfseries\sffamily}
	\titleformat*{\subsection}{\Large\bfseries\sffamily}
	\titleformat*{\subsubsection}{\large\bfseries\sffamily}
	\titleformat*{\paragraph}{\large\bfseries\sffamily}
	\titleformat*{\subparagraph}{\large\bfseries\sffamily}
\makeatother


% captions for figures and tables
\RequirePackage{caption}
\makeatletter
	\renewcommand{\captionlabelfont}{\bfseries}
	\setlength{\captionmargin}{2cm}
\makeatother

% takes care of the abstract and aknowledgements
\RequirePackage{abstract}

\makeatletter

\newcommand{\keywords}[1]{\def \@keywords {#1}}
\newcommand{\nyckelord}[1]{\def \@nyckelord {#1}}
\newcommand{\theabstract}[1]{\def \@theabstract {#1}}
\newcommand{\sammanfattning}[1]{\def \@sammanfattning {#1}}
\newcommand{\acknowledgements}[1]{\def \@acknowledgements {#1}}

\keywords{}
\nyckelord{}
\theabstract{}
\sammanfattning{}
\acknowledgements{}

% indent the abstract properly
\setlength{\absrightindent}{1cm}
\setlength{\absleftindent}{1cm}

% makes everything in the begining of the document
\newcommand{\makefrontmatter}{
	% the title page
	\maketitle
	
	% make the copyright
	\pagestyle{empty}
	\par Copyright \copyright \the\year , \space \@author \vskip 1em
	\textit{Published by} \vskip 1em
	\par \ifx\@department\empty
		 	Certec, Dept. of Design Sciences, Lund University.
		\else \@department. \fi
	\par Faculty of Engineering LTH, Lund University 
	\par P.O. Box 118, SE-221 00 Lund, Sweden \vskip 4em
	\par \ifx\@subject\empty
		\else Subject: \@subject \fi
	\par Division: \ifx\@department\empty
		 	Certec, Dept. of Design Sciences, Lund University.
		\else \@department. \fi
	\par Supervisor: \@firstsupervisor
	\par Co-supervisor: \@secondsupervisor
	\par Examiner: \@examiner
	
	% make the abstract and keywords
	\ifx \@theabstract \@empty
		\ClassWarning{certec-msc}{The abstract is missing. Use \protect\theabstract{text}.}
	\else
		% empty page		
		\pagestyle{empty}
		% make sure the abstract is on a right-hand side
		\cleardoublepage
		\begin{abstract}
		\noindent
		\@theabstract		
		\ifx \@keywords \@empty
			\ClassWarning{certec-msc}{Keywords are missing. Use \protect\keywords{words}.}
		\else
			\vspace{2cm}\par\noindent {\small{\bf Keywords\/}: \@keywords}
		\fi
		\end{abstract}
	\fi
	
	% make the abstract and keywords in swedish
	\ifx \@sammanfattning \@empty
		\ClassWarning{certec-msc}{The swedish abstract is missing. Use \protect\sammanfattning{text}.}
	\else
		% empty page		
		\pagestyle{empty}
		% make sure the abstract is on a right-hand side
		\cleardoublepage
		\renewcommand{\abstractname}{Sammanfattning}
		\begin{abstract}
		\noindent
		\@sammanfattning		
		\ifx \@nyckelord \@empty
			\ClassWarning{certec-msc}{Keywords in swedish are missing. Use \protect\nyckelord{words}.}
		\else
			\vspace{2cm}\par\noindent {\small{\bf Nyckelord\/}: \@nyckelord}
		\fi
		\end{abstract}
	\fi
	
	
	\pagestyle{fancy}

	% add acknowledgement if any
	\ifx \@acknowledgements \@empty
			\relax
	\else
			\chapter*{Acknowledgements}
			\@acknowledgements
	\fi

	\tableofcontents
}	
\makeatother


% make the bibliography
\makeatletter
\newcommand{\makebibliography}[1]{
	\cleardoublepage
	\phantomsection
	\addcontentsline{toc}{chapter}{Bibliography}
	\bibliographystyle{plain}
	\bibliography{#1}
}