# README #

This repository contains the LaTeX class and a template for generating a Master's Thesis report carried out at Certec, Department of Design Sciences, Lund University. It is based on a fork from the Department of Computer Science, Lund University.

### What is this repository for? ###

* Suggested format for a Master's Thesis at DS@LTH
* Version: Oct 13, 2016
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I use it? ###

* Have a look at the MScTemplateDS.pdf included in this repository
* Download and use with your LaTeX

### Contribution guidelines ###

* Talk to Flavius@CS to get access to the original template